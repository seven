/* {{{ irc-seven: Cows like it.
 *
 * Copyright (C) 1990 Jarkko Oikarinen and University of Oulu, Co Center.
 * Copyright (C) 1996-2002 Hybrid Development Team.
 * Copyright (C) 2002-2005 ircd-ratbox development team.
 * Copyright (C) 2005 William Pitcock and Jilles Tjoelker.
 *
 * Based on mkpasswd.c, originally by Nelson Minar (minar@reed.edu).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to:
 *
 *	Free Software Foundation, Inc.
 *	51 Franklin St - Fifth Floor
 *	Boston, MA 02110-1301
 *	USA
 *
 * }}} */

#ifndef LIBSEVEN_H
# define LIBSEVEN_H

/* {{{ Includes. */
# include "stdinc.h"
# include "res.h"
# include "numeric.h"
# include "tools.h"
# include "memory.h"
# include "balloc.h"
# include "linebuf.h"
# include "sprintf_irc.h"
# include "commio.h"
# include "event.h"
/* }}} */

typedef void (*seven_log_cb) (const char *);

extern void libseven_log (const char *str, ...);
extern void libseven_restart (const char *str, ...);
extern void libseven_die (const char *str, ...);
extern void libseven_init (seven_log_cb, seven_log_cb, seven_log_cb);

#endif /* ! LIBSEVEN_H */

/*
 * vim: ts=8 sw=8 noet fdm=marker tw=80
 */
