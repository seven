/* {{{ irc-seven: Cows like it.
 *
 * Copyright (C) 1990 Jarkko Oikarinen and University of Oulu, Co Center.
 * Copyright (C) 1996-2002 Hybrid Development Team.
 * Copyright (C) 2002-2005 ircd-ratbox development team.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to:
 *
 *	Free Software Foundation, Inc.
 *	51 Franklin St - Fifth Floor
 *	Boston, MA 02110-1301
 *	USA
 *
 * }}} */

/* {{{ Includes. */
#include "stdinc.h"
#include "client.h"
#include "irc_string.h"
#include "s_serv.h"
#include "s_conf.h"
#include "msg.h"
#include "parse.h"
#include "modules.h"
/* }}} */

static int mr_capab (struct Client *, struct Client *, int, const char **);
static int me_gcap (struct Client *, struct Client *, int, const char **);

/* {{{ static struct Message capab_msgtab = { ... } */
static struct Message capab_msgtab =
{
	"CAPAB", 0, 0, 0, MFLG_SLOW | MFLG_UNREG,
	{
		{mr_capab, 0}, mg_ignore, mg_ignore,
		mg_ignore, mg_ignore, mg_ignore,
	},
};
/* }}} */

/* {{{ static struct Message gcap_msgtab = { ... } */
static struct Message gcap_msgtab =
{
	"GCAP", 0, 0, 0, MFLG_SLOW,
	{
		mg_ignore, mg_ignore, mg_ignore,
		mg_ignore, {me_gcap, 2}, mg_ignore,
	},
};
/* }}} */

/* {{{ static mapi_clist_av1 capab_clist[] = { ... } */
static mapi_clist_av1 capab_clist[] =
{
	&capab_msgtab,
	&gcap_msgtab,
	NULL
};
/* }}} */

/* {{{ DECLARE_MODULE_AV1(...) */
DECLARE_MODULE_AV1
(
	capab,
	NULL,
	NULL,
	capab_clist,
	NULL,
	NULL,
	"1.1"
);
/* }}} */

/* {{{ static int mr_capab()
 *
 * CAPAB message handler.
 *
 * Usage:
 *	CAPAB <capabs>
 *
 * parv[0] - prefix
 * parv[1] - capabs
 */
static int
mr_capab (struct Client *client_p, struct Client *source_p, int parc, const char **parv)
{
	struct Capability	*cap = NULL;
	int			i;
	char			*p = NULL;
	char			*s = NULL;

	if (!client_p->localClient || client_p->user)
		return 0;

	if ((client_p->localClient->caps & ~CAP_TS6) != 0)
	{
		exit_client(client_p, client_p, client_p,
			"CAPAB TS6 is specified via PASS not CAPAB");
		return 0;
	}

	client_p->localClient->caps |= CAP_CAP;
	MyFree(client_p->localClient->fullcaps);
	DupString(client_p->localClient->fullcaps, parv[1]);

	for (i = 1; i < parc; ++i)
	{
		char *t = LOCAL_COPY(parv[i]);
		for (s = strtoken(&p, t, " "); s; s = strtoken(&p, NULL, " "))
		{
			for (cap = captab; cap->name; cap++)
			{
				if (!irccmp(cap->name, s))
				{
					client_p->localClient->caps |= cap->cap;
					break;
				}
			}
		}
	}

	return 0;
}
/* }}} */

/* {{{ static int me_gcap()
 *
 * GCAP ENCAP message handler.
 *
 * Usage:
 *	GCAP <capabs>
 *
 * parv[0] - prefix
 * parv[1] - capabs
 */
static int
me_gcap (struct Client *client_p, struct Client *source_p, int parc, const char **parv)
{
	struct Capability	*cap = NULL;
	char			*t = LOCAL_COPY(parv[1]);
	char			*s = NULL;
	char			*p = NULL;

	if (!IsServer(source_p) || !EmptyString(source_p->serv->fullcaps))
		return 0;

	DupString(source_p->serv->fullcaps, parv[1]);
	for (s = strtoken(&p, t, " "); s; s = strtoken(&p, NULL, " "))
	{
		for (cap = captab; cap->name; cap++)
		{
			if (!irccmp(cap->name, s))
			{
				source_p->serv->caps |= cap->cap;
				break;
			}
		}
	}

	return 0;
}
/* }}} */

/*
 * vim: ts=8 sw=8 noet fdm=marker tw=80
 */
