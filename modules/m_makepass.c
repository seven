/* {{{ irc-seven: Cows like it.
 *
 * Copyright (C) 2002 W. Campbell and the ircd-ratbox development team.
 *
 * Based on mkpasswd.c, originally by Nelson Minar (minar@reed.edu).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to:
 *
 *	Free Software Foundation, Inc.
 *	51 Franklin St - Fifth Floor
 *	Boston, MA 02110-1301
 *	USA
 *
 * }}} */

/* {{{ Includes. */
#include "stdinc.h"
#include "client.h"
#include "common.h"
#include "ircd.h"
#include "irc_string.h"
#include "numeric.h"
#include "patricia.h"
#include "s_newconf.h"
#include "s_conf.h"
#include "s_log.h"
#include "s_serv.h"
#include "send.h"
#include "msg.h"
#include "parse.h"
#include "modules.h"
/* }}} */

extern char *crypt();

static int m_makepass (struct Client *, struct Client *, int, const char **);

static char *make_md5_salt (void);

static char salt_chars[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789./";

/* {{{ static struct Message makepass_msgtab = { ... } */
struct Message makepass_msgtab =
{
	"MKPASSWD", 0, 0, 0, MFLG_SLOW,
	{
		mg_unreg, {m_makepass, 0}, mg_ignore,
		mg_ignore, mg_ignore, {m_makepass, 0},
	},
};
/* }}} */

mapi_clist_av1 makepass_clist[] =
{
	&makepass_msgtab,
	NULL,
};

/* {{{ DECLARE_MODULE_AV1(...) */
DECLARE_MODULE_AV1
(
	makepass,
	NULL,
	NULL,
	makepass_clist,
	NULL,
	NULL,
	"1.1"
);
/* }}} */

/* {{{ m_makepass()
 *
 * Usage:
 * 	MAKEPASS <password>
 *
 * parv[0] - prefix
 * parv[1] - password
 */
static int
m_makepass (struct Client *client_p, struct Client *source_p, int parc, const char **parv)
{
	static time_t last_used = 0;

	if (parc < 2 || EmptyString(parv[1]))
	{
		sendto_one(source_p, form_str(ERR_NEEDMOREPARAMS), me.name,
			source_p->name, "MAKEPASS");
		return 0;
	}
	else if ((last_used + ConfigFileEntry.pace_wait) > CurrentTime)
	{
		sendto_one(source_p, form_str(RPL_LOAD2HI), me.name, parv[0]);
		return 0;
	}

	last_used = CurrentTime;
	sendto_one(source_p, ":%s NOTICE %s :Encryption for [ %s ] is [ %s ]",
		me.name, source_p->name, parv[1],
		crypt(parv[1], make_md5_salt()));

	return 0;
}
/* }}} */

/* {{{ make_md5_salt()
 *
 * Create random salt to be used with MD5-based crypt(3).
 */
static char *
make_md5_salt(void)
{
	static char	salt[13];
	int		i = 0;

	salt[i++] = '$';
	salt[i++] = '1';
	salt[i++] = '$';

	while (i < 11)
		salt[i++] = salt_chars[random() % 64];

	salt[i++] = '$';
	salt[i++] = '\0';

	return salt;
}
/* }}} */

/*
 * vim: ts=8 sw=8 noet fdm=marker tw=80
 */
