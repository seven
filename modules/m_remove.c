#include "stdinc.h"
#include "modules.h"
#include "client.h"
#include "ircd.h"
#include "send.h"
#include "numeric.h"
#include "s_serv.h"
#include "hash.h"
#include "channel.h"
#include "packet.h"

#define mg_remove {m_remove, 0}

static int m_remove (struct Client *client_p, struct Client *source_p, int parc, const char **parv);

/* {{{ struct Message remove_msgtab = { ... } */
struct Message remove_msgtab =
{
	"REMOVE", 0, 0, 0, MFLG_SLOW,
	{
		mg_unreg,
		mg_remove,
		mg_remove,
		mg_remove,
		mg_ignore,
		mg_remove,
	},
};
/* }}} */

/* {{{ mapi_clist_av1 remove_clist[] = { ... } */
mapi_clist_av1 remove_clist[] =
{
	&remove_msgtab,
	NULL,
};
/* }}} */

/* {{{ DECLARE_MODULE_AV1(...) */
DECLARE_MODULE_AV1
(
	remove,
	NULL,
	NULL,
	remove_clist,
	NULL,
	NULL,
	"$Revision$"
);
/* }}} */

/* {{{ static int m_remove()
 *
 * REMOVE <channel> <nick> [<reason>]
 *
 * parv[0] - prefix
 * parv[1] - channel
 * parv[2] - nick
 * parv[3] - reason
 *
 */
static int
m_remove (struct Client *client_p, struct Client *source_p, int parc, const char **parv)
{
	uint8_t			have_reason = 0;
	char			reason_buf[REASONLEN + 1];
	const char		*p = NULL;
	struct Channel		*chptr = NULL;
	struct membership	*msptr = NULL;
	struct Client		*target_p = NULL;
	int			chasing = 0;

	if (MyClient(source_p) && !IsFloodDone(source_p))
		flood_endgrace(source_p);

	if (parc < 3)
	{
		sendto_one(source_p, form_str(ERR_NEEDMOREPARAMS), me.name, source_p->name, "REMOVE");
		return 0;
	}

	if (parc > 3 && *parv[3])
		have_reason = 1;

	ircsnprintf(reason_buf, sizeof(reason_buf), "requested by %s%s%s%s",
		source_p->name,
		!have_reason ? "" : ": \"",
		!have_reason ? "" : parv[3],
		!have_reason ? "" : "\"");
	reason_buf[REASONLEN] = '\0';

	p = strchr(parv[1], ',');
	if (!p)
		p = parv[1];

	chptr = find_channel(p);
	if (!p)
	{
		sendto_one(source_p, form_str(ERR_NOSUCHCHANNEL), me.name, source_p->name, p);
		return 0;
	}

	if (!IsServer(source_p) && !IsOverride(source_p))
	{
		msptr = find_channel_membership(chptr, source_p);
		if (!msptr && MyConnect(source_p))
		{
			sendto_one(source_p, form_str(ERR_NOTONCHANNEL), me.name, source_p->name, p);
			return 0;
		}

		/*
		 * Nicked from m_kick.c; see that file for reasoning behind the
		 * code below...
		 */

		if(!is_chanop(msptr))
		{
			if(MyConnect(source_p))
			{
				sendto_one(source_p, form_str(ERR_CHANOPRIVSNEEDED),
					   me.name, source_p->name, p);
				return 0;
			}

			/* If its a TS 0 channel, do it the old way */
			if(chptr->channelts == 0)
			{
				sendto_one(source_p, form_str(ERR_CHANOPRIVSNEEDED),
					   get_id(&me, source_p), get_id(source_p, source_p), p);
				return 0;
			}
		}
	}

	p = strchr(parv[2], ',');
	if (!p)
		p = parv[2];

	target_p = find_chasing(source_p, p, &chasing);
	if (!target_p)
		return 0;

	msptr = find_channel_membership(chptr, target_p);
	if (!msptr)
	{
		sendto_one_numeric(source_p, ERR_USERNOTINCHANNEL, form_str(ERR_USERNOTINCHANNEL),
			p, chptr->chname);
		return 0;
	}

	if (MyClient(source_p))
	{
		if (IsService(target_p))
		{
			sendto_one(source_p, form_str(ERR_ISCHANSERVICE), me.name, source_p->name,
				p, chptr->chname);
			return 0;
		}
		else if (IsOverride(target_p))
		{
			sendto_one_numeric(source_p, ERR_ISCHANSERVICE, 
				"%s %s User is immune from kick", target_p->name, chptr->chname);
			return 0;
		}
	}

	sendto_channel_local(ALL_MEMBERS, chptr, ":%s!%s@%s PART %s :%s",
		target_p->name, target_p->username, target_p->host,
		chptr->chname, reason_buf);

	/* propagate to TS6 servers that support REMOVE. */
	sendto_server(client_p, chptr, CAP_TS6 | CAP_REMOVE, NOCAPS,
		":%s REMOVE %s %s %s%s", use_id(source_p), chptr->chname, use_id(target_p), 
		!have_reason ? "" : ":",
		!have_reason ? "" : parv[3]);

	/* ...and to those that do not, translating to KICK. */
	sendto_server(client_p, chptr, CAP_TS6, NOCAPS,
		":%s KICK %s %s %s%s",
		use_id(source_p), chptr->chname, use_id(target_p),
		!have_reason ? "" : ":",
		!have_reason ? "" : parv[3]);
	sendto_server(client_p, chptr, NOCAPS, CAP_TS6,
		":%s KICK %s %s %s%s",
		source_p->name, chptr->chname, target_p->name,
		!have_reason ? "" : ":",
		!have_reason ? "" : parv[3]);

	remove_user_from_channel(msptr);

	return 0;
}
/* }}} */

/*
 * vim: ts=8 sw=8 noet fdm=marker tw=80
 */
