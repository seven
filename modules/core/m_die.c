/* {{{ irc-seven: Cows like it.
 *
 * Copyright (C) 1990 Jarkko Oikarinen and University of Oulu, Co Center
 * Copyright (C) 1996-2002 Hybrid Development Team
 * Copyright (C) 2002-2005 ircd-ratbox development team
 * Copyright (c) 2006 Elfyn McBratney.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to:
 *
 *	Free Software Foundation, Inc.
 *	51 Franklin St - Fifth Floor
 *	Boston, MA 02110-1301
 *	USA
 *
 * $Id: m_die.c 195 2006-12-19 01:11:35Z beu $ }}} */

/* {{{ Includes. */
#include "stdinc.h"
#include "tools.h"
#include "client.h"
#include "ircd.h"
#include "irc_string.h"
#include "numeric.h"
#include "commio.h"
#include "s_log.h"
#include "s_conf.h"
#include "s_newconf.h"
#include "s_serv.h"
#include "send.h"
#include "msg.h"
#include "parse.h"
#include "modules.h"
#include "hash.h"
/* }}} */

static int mo_die (struct Client *, struct Client *, int, const char **);
static int me_die (struct Client *, struct Client *, int, const char **);

static int do_die (struct Client *, const char *, uint8_t);

/* {{{ static struct Message die_msgtab = { ... } */
static struct Message die_msgtab =
{
	"DIE", 0, 0, 0, MFLG_SLOW,
	{
		mg_unreg, mg_not_oper, mg_ignore,
		mg_ignore, {me_die, 0}, {mo_die, 0},
	},
};
/* }}} */

mapi_clist_av1 die_clist[] =
{
	&die_msgtab,
	NULL,
};

/* {{{ DECLARE_MODULE_AV1(...) */
DECLARE_MODULE_AV1
(
	die,
	NULL,
	NULL,
	die_clist,
	NULL,
	NULL,
	"$Revision: 195 $"
);
/* }}} */

/* {{{ mo_die()
 *
 * DIE message handler.
 *
 * Usage:
 * 	DIE <server>
 *
 * parv[0] - prefix
 * parv[1] - server
 */
static int
mo_die (struct Client *client_p, struct Client *source_p, int parc, const char **parv)
{
	uint8_t is_me;

	if (!IsOperDie(source_p))
	{
		sendto_one(source_p, form_str(ERR_NOPRIVS), me.name,
			source_p->name, "die");
		return 0;
	}
	else if (parc < 2 || EmptyString(parv[1]))
	{
		sendto_one(source_p, form_str(ERR_NEEDMOREPARAMS), me.name,
			source_p->name, "DIE");
		return 0;
	}
	else if (strchr(parv[1], '?') || strchr(parv[1], '*'))
	{
		sendto_one(source_p,
			":%s NOTICE %s :Argument to DIE cannot contain wildcards",
			me.name, source_p->name);
		return 0;
	}
	else if (!find_server(source_p, parv[1]))
	{
		sendto_one_numeric(source_p, ERR_NOSUCHSERVER,
			form_str(ERR_NOSUCHSERVER), parv[1]);
		return 0;
	}

	is_me = irccmp(parv[1], me.name) == 0 ? 1 : 0;
	if (!is_me && !ConfigFileEntry.remote_die)
	{
		sendto_one(source_p,
			":%s NOTICE %s :Argument to DIE must local server name (%s)",
			me.name, source_p->name, me.name);
		return 0;
	}

	return do_die(source_p, parv[1], !is_me);
}
/* }}} */

/* {{{ me_die()
 *
 * DIE ENCAP message handler.
 *
 * parv[0] - prefix
 */
static int
me_die (struct Client *client_p, struct Client *source_p, int parc, const char **parv)
{
	if (!IsPerson(source_p) || !ConfigFileEntry.remote_die)
		return 0;
	if (!find_client_shared_conf(source_p, SHARED_DIE))
		return 0;

	return do_die(source_p, NULL, 0);
}
/* }}} */

/* {{{ do_die()
 *
 * Restart local server or request that a remote server restart.
 */
static int
do_die (struct Client *source_p, const char *target, uint8_t remote)
{
	dlink_node	*cur = NULL;
	struct Client	*target_p = NULL;

	if (remote)
	{
		sendto_realops_snomask(SNO_GENERAL, L_NETWIDE,
			"%s is requesting shutdown of %s",
			get_oper_name(source_p), target);
		sendto_match_servs(source_p, target, CAP_ENCAP, NOCAPS,
			"ENCAP %s DIE", target);

		return 0;
	}

	sendto_realops_snomask(SNO_GENERAL, L_NETWIDE, "%s is shutting down %s...",
			get_oper_name(source_p), me.name);

	DLINK_FOREACH (cur, lclient_list.head)
	{
		target_p = cur->data;
		sendto_one(target_p,
			":%s NOTICE %s :Server Terminating.",
			me.name, target_p->name);
	}

	DLINK_FOREACH (cur, serv_list.head)
	{
		target_p = cur->data;
		sendto_one(target_p, ":%s ERROR :Terminated by %s",
			me.name, get_client_name(source_p, HIDE_IP));
	}

	ilog(L_MAIN, "Server terminated by %s", get_oper_name(source_p));
	unlink(pidFileName);
	exit(0);

	/* not reached. */
	return 0;
}
/* }}} */

/*
 * vim: ts=8 sw=8 noet fdm=marker tw=80
 */
