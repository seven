/* {{{ irc-seven: Cows like it.
 *
 * Copyright (C) 1990 Jarkko Oikarinen and University of Oulu, Co Center
 * Copyright (C) 1996-2002 Hybrid Development Team
 * Copyright (C) 2002-2005 ircd-ratbox development team
 * Copyright (c) 2006 Elfyn McBratney.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to:
 *
 *	Free Software Foundation, Inc.
 *	51 Franklin St - Fifth Floor
 *	Boston, MA 02110-1301
 *	USA
 *
 * }}} */

/* {{{ Includes. */
#include "stdinc.h"
#include "client.h"
#include "common.h"
#include "irc_string.h"
#include "sprintf_irc.h"
#include "ircd.h"
#include "numeric.h"
#include "s_conf.h"
#include "s_newconf.h"
#include "s_log.h"
#include "s_serv.h"
#include "restart.h"
#include "send.h"
#include "msg.h"
#include "parse.h"
#include "modules.h"
#include "hash.h"
/* }}} */

static int mo_restart (struct Client *, struct Client *, int, const char **);
static int me_restart (struct Client *, struct Client *, int, const char **);
 
static int do_restart (struct Client *, const char *, uint8_t);

/* {{{ struct Message restart_msgtab = { ... } */
struct Message restart_msgtab =
{
	"RESTART", 0, 0, 0, MFLG_SLOW,
	{
		mg_unreg, mg_not_oper, mg_ignore,
		mg_ignore, {me_restart, 0}, {mo_restart, 0},
	},
};
/* }}} */

mapi_clist_av1 restart_clist[] =
{
	&restart_msgtab,
	NULL,
};

/* {{{ DECLARE_MODULE_AV1(...) */
DECLARE_MODULE_AV1
(
	restart,
	NULL,
	NULL,
	restart_clist,
	NULL,
	NULL,
	"$Revision: 195 $"
);
/* }}} */

/* {{{ mo_restart()
 *
 * RESTART message handler.
 *
 * Usage:
 *	RESTART <target>
 *
 * parv[0] - prefix
 * parv[1] - target
 */
static int
mo_restart (struct Client *client_p, struct Client *source_p, int parc, const char **parv)
{
	uint8_t is_me;

	if (!IsOperDie(source_p))
	{
		sendto_one(source_p, form_str(ERR_NOPRIVS), me.name,
			source_p->name, "die");
		return 0;
	}
	else if (parc < 2 || EmptyString(parv[1]))
	{
		sendto_one(source_p, form_str(ERR_NEEDMOREPARAMS), me.name,
			source_p->name, "RESTART");
		return 0;
	}
	else if (strchr(parv[1], '?') || strchr(parv[1], '*'))
	{
		sendto_one(source_p,
			":%s NOTICE %s :Argument to RESTART cannot contain wildcards",
			me.name, source_p->name);
		return 0;
	}
	else if (!find_server(source_p, parv[1]))
	{
		sendto_one_numeric(source_p, ERR_NOSUCHSERVER,
			form_str(ERR_NOSUCHSERVER), parv[1]);
		return 0;
	}

	is_me = irccmp(parv[1], me.name) == 0 ? 1 : 0;
	if (!is_me && !ConfigFileEntry.remote_restart)
	{
		sendto_one(source_p,
			":%s NOTICE %s :Argument to RESTART must local server name (%s)",
			me.name, source_p->name, me.name);
		return 0;
	}

	return do_restart(source_p, parv[1], !is_me);
}
/* }}} */

/* {{{ me_restart()
 *
 * parv[0] - prefix
 */
static int
me_restart (struct Client *client_p, struct Client *source_p, int parc, const char **parv)
{
	if (!IsPerson(source_p) || !ConfigFileEntry.remote_restart)
		return 0;
	if (!find_client_shared_conf(source_p, SHARED_DIE))
		return 0;

	return do_restart(source_p, NULL, 0);
}
/* }}} */

/* {{{ do_restart()
 *
 * Restart local server or request that a remote server restart.
 */
static int
do_restart (struct Client *source_p, const char *target, uint8_t remote)
{
	dlink_node	*cur = NULL;
	struct Client	*target_p = NULL;
	char		buf[BUFSIZE];

	if (remote)
	{
		sendto_realops_snomask(SNO_GENERAL, L_NETWIDE,
			"%s is requesting restart of %s",
			get_oper_name(source_p), target);
		sendto_match_servs(source_p, target, CAP_ENCAP, NOCAPS,
			"ENCAP %s RESTART", target);

		return 0;
	}

	sendto_realops_snomask(SNO_GENERAL, L_NETWIDE, "%s is restarting %s...",
		get_oper_name(source_p), me.name);

	DLINK_FOREACH (cur, lclient_list.head)
	{
		target_p = cur->data;
		sendto_one(target_p,
			":%s NOTICE %s :Server Restarting.",
			me.name, target_p->name);
	}

	DLINK_FOREACH (cur, serv_list.head)
	{
		target_p = cur->data;
		sendto_one(target_p, ":%s ERROR :Restart by %s",
			me.name, get_client_name(source_p, HIDE_IP));
	}

	ircsprintf(buf, "Server RESTART by %s", get_client_name(source_p, HIDE_IP));
	restart(buf);

	return 0;
}
/* }}} */

/*
 * vim: ts=8 sw=8 noet fdm=marker tw=80
 */
