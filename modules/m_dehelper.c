/*
 * ircd-seven: a bovine ircd
 * m_dehelper.c: Remove a given user from the /stats p list.
 *
 * Copyright (C) 2006 Stephen Bennett <spb@gentoo.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 *
 *  $Id: $
 */

#include "stdinc.h"
#include "client.h"
#include "msg.h"
#include "modules.h"
#include "numeric.h"
#include "s_user.h"
#include "s_conf.h"

static int do_dehelper(struct Client *, struct Client *);
static int mo_dehelper(struct Client *, struct Client *, int, const char **);
static int me_dehelper(struct Client *, struct Client *, int, const char **);

struct Message dehelper_msgtab = {
	"DEHELPER", 0, 0, 0, MFLG_SLOW,
	{mg_unreg, mg_not_oper, mg_ignore, mg_ignore, {me_dehelper, 2}, {mo_dehelper, 2} }
};

mapi_clist_av1 dehelper_clist[] = { &dehelper_msgtab, NULL };

DECLARE_MODULE_AV1(dehelper, NULL, NULL, dehelper_clist, NULL, NULL, "$Revision: $");

int mo_dehelper(struct Client *client_p, struct Client *source_p, int parc, const char **parv)
{
	struct Client *target_p;

	if (!IsOperStaffer(source_p))
		sendto_one(source_p, form_str(ERR_NOPRIVS), me.name, source_p->name, "dehelper");

	if (!(target_p = find_named_person(parv[1])))
	{
		sendto_one(source_p, form_str(ERR_NOSUCHNICK), parv[1]);
		return 0;
	}

	if (MyClient(target_p))
		do_dehelper(source_p, target_p);
	else
		sendto_one(target_p, ":%s ENCAP %s DEHELPER %s",
				get_id(source_p, target_p), target_p->servptr->name,
				get_id(target_p, target_p));

	return 0;
}

int me_dehelper(struct Client *client_p, struct Client *source_p, int parc, const char **parv)
{
	struct Client *target_p = find_person(parv[1]);
	if (MyClient(target_p))
		do_dehelper(source_p, target_p);

	return 0;
}

int do_dehelper(struct Client *source_p, struct Client *target_p)
{
	const char *newparv[4];

	newparv[0] = newparv[1] = target_p->name;
	newparv[2] = "-T";
	newparv[3] = NULL;
	
	sendto_realops_snomask(SNO_GENERAL, L_NETWIDE, "%s is using DEHELPER on %s",
			get_oper_name(source_p), get_client_name(target_p, HIDE_IP));
	sendto_one_notice(target_p, ":%s is dehelpering you", source_p->name);
	user_mode(target_p, target_p, 3, newparv);
	sendto_one_notice(source_p, ":Dehelpered %s", target_p->name);

	return 0;
}
