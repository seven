/* {{{ irc-seven: Cows like it.
 *
 * Copyright (C) 1990 Jarkko Oikarinen and University of Oulu, Co Center.
 * Copyright (C) 1996-2002 Hybrid Development Team.
 * Copyright (C) 2002-2005 ircd-ratbox development team.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to:
 *
 *	Free Software Foundation, Inc.
 *	51 Franklin St - Fifth Floor
 *	Boston, MA 02110-1301
 *	USA
 *
 * }}} */

/* {{{ Includes. */
#include "stdinc.h"
#include "client.h"
#include "ircd.h"
#include "numeric.h"
#include "s_conf.h"
#include "s_serv.h"
#include "send.h"
#include "msg.h"
#include "parse.h"
#include "hook.h"
#include "modules.h"
/* }}} */

static int m_admin (struct Client *, struct Client *, int, const char **);
static int mr_admin (struct Client *, struct Client *, int, const char **);
static int ms_admin (struct Client *, struct Client *, int, const char **);

static int do_admin (struct Client *source_p);

/* {{{ static struct Message = { ... } */
static struct Message admin_msgtab =
{
	"ADMIN", 0, 0, 0, MFLG_SLOW | MFLG_UNREG,
	{
		{mr_admin, 0}, {m_admin, 0}, {ms_admin, 0},
		mg_ignore, mg_ignore, {ms_admin, 0},
	},
};
/* }}} */

/* {{{ mapi_clist_av1 admin_clist[] = { ... } */
mapi_clist_av1 admin_clist[] =
{
	&admin_msgtab,
	NULL,
};
/* }}} */

/* {{{ DECLARE_MODULE_AV1(...) */
DECLARE_MODULE_AV1
(
	admin,
	NULL,
	NULL,
	admin_clist,
	NULL,
	NULL,
	"1.1"
);
/* }}} */

/* {{{ static int mr_admin()
 *
 * ADMIN message handler.
 *
 * Usage:
 *	ADMIN
 *
 * parv[0] - prefix
 */
static int
mr_admin (struct Client *client_p, struct Client *source_p, int parc, const char **parv)
{
	static time_t last_used = 0;

	if ((last_used + ConfigFileEntry.pace_wait) > CurrentTime)
	{
		sendto_one(source_p, form_str(RPL_LOAD2HI), me.name,
			EmptyString(source_p->name) ? "*" : source_p->name,
			"ADMIN");
		return 0;
	}
	else
		last_used = CurrentTime;

	return do_admin(source_p);
}
/* }}} */

/* {{{ static int m_admin()
 *
 * ADMIN message handler.
 *
 * Usage:
 *	ADMIN [<server>]
 *
 * parv[0] - prefix
 * parv[1] - server
 */
static int
m_admin (struct Client *client_p, struct Client *source_p, int parc, const char **parv)
{
	static time_t	last_used = 0;
	int		hunted;

	if (parc > 1)
	{
		if ((last_used + ConfigFileEntry.pace_wait) > CurrentTime)
		{
			sendto_one(source_p, form_str(RPL_LOAD2HI), me.name,
				source_p->name, "ADMIN");
			return 0;
		}
		else
			last_used = CurrentTime;
		
		hunted = hunt_server(client_p, source_p, ":%s ADMIN :%s", 1, parc, parv);
		if (hunted != HUNTED_ISME)
			return 0;
	}

	return do_admin(source_p);
}
/* }}} */

/* {{{ static int ms_admin()
 *
 * ADMIN oper/server message handler.
 *
 * Usage:
 *	ADMIN [<server>]
 *
 * parv[0] - prefix
 * parv[1] - server
 */
static int
ms_admin (struct Client *client_p, struct Client *source_p, int parc, const char **parv)
{
	int hunted;

	hunted = hunt_server(client_p, source_p, ":%s ADMIN :%s", 1, parc, parv);
	if (hunted != HUNTED_ISME)
		return 0;

	return do_admin(source_p);
}
/* }}} */

/* {{{ static int do_admin()
 *
 * Perform actual ADMIN work.
 */
static int
do_admin (struct Client *source_p)
{
	const char *myname = NULL;
	const char *nick = NULL;

	myname = get_id(&me, source_p);
	nick = EmptyString(source_p->name) ? "*" : get_id(source_p, source_p);

	sendto_one(source_p, form_str(RPL_ADMINME), myname, nick, me.name);
	if(AdminInfo.name != NULL)
		sendto_one(source_p, form_str(RPL_ADMINLOC1), myname, nick,
			AdminInfo.name);
	if(AdminInfo.description != NULL)
		sendto_one(source_p, form_str(RPL_ADMINLOC2), myname, nick,
			AdminInfo.description);
	if(AdminInfo.email != NULL)
		sendto_one(source_p, form_str(RPL_ADMINEMAIL), myname, nick,
			AdminInfo.email);

	return 0;
}
/* }}} */

/*
 * vim: ts=8 sw=8 noet fdm=marker tw=80
 */
