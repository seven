/* {{{ irc-seven: Cows like it.
 *
 * Copyright (C) 1990 Jarkko Oikarinen and University of Oulu, Co Center.
 * Copyright (C) 1996-2002 Hybrid Development Team.
 * Copyright (C) 2002-2004 ircd-ratbox development team.
 * Copyright (C) 2005-2006 Charybdis development team.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to:
 *
 *	Free Software Foundation, Inc.
 *	51 Franklin St - Fifth Floor
 *	Boston, MA 02110-1301
 *	USA
 *
 * }}} */

/*
 * ircd_defs.h - Global size definitions for record entries used
 * througout ircd-seven.  Please think 3 times before adding anything
 * to this file.
 */

#ifndef _SEVEN_IRCD_DEFS_H
# define _SEVEN_IRCD_DEFS_H

# include "config.h"

# ifdef __GNUC__
#  define AFP(a, b) __attribute__((format(printf, a, b)))
# else
#  define AFP(a, b)
# endif

# include "s_log.h"
# include "send.h"

/* {{{ #define s_assert(expr) */
# ifdef SOFT_ASSERT
#  ifdef __GNUC__
#   define s_assert(expr)								\
	do										\
	{										\
		if (!(expr))								\
		{									\
			ilog(L_MAIN,							\
				"file: %s line: %d (%s): Assertion failed: (%s)",	\
				__FILE__, __LINE__, __PRETTY_FUNCTION__, #expr); 	\
			sendto_realops_snomask(SNO_GENERAL, L_ALL, 			\
				"file: %s line: %d (%s): Assertion failed: (%s)",	\
				__FILE__, __LINE__, __PRETTY_FUNCTION__, #expr);	\
		}									\
	}										\
	while(0)
#  else
#   define s_assert(expr)								\
	do										\
	{										\
		if (!(expr))								\
		{									\
			ilog(L_MAIN,							\
				"file: %s line: %d (%s): Assertion failed: (%s)",	\
				__FILE__, __LINE__, __FUNCTION__, #expr); 		\
			sendto_realops_snomask(SNO_GENERAL, L_ALL, 			\
				"file: %s line: %d (%s): Assertion failed: (%s)",	\
				__FILE__, __LINE__, __FUNCTION__, #expr);		\
		}									\
	}										\
	while(0)
#  endif
# else
#  define s_assert(expr) assert(expr)
# endif
/* }}} */

# if !defined(CONFIG_RATBOX_LEVEL_1)
#  error Incorrect config.h for this revision of ircd.
# endif

/*
 * This defines the version of the data structures used in the ircd.
 * In the event of a mismatch (i.e. this is incremented due to a major
 * change that cannot be accomidated for in the ircd), then a hard
 * restart occurs.
 */
# define SEVEN_DV	0x00000900 /* 0.9.0 */

/*
 * Change the below definition of BUFSIZE and expect bad things to happen...
 */
# define BUFSIZE 512

# define HOSTLEN	63
# define USERLEN	10
# define REALLEN	50
# define CHANNELLEN	200
# define LOC_CHANNELLEN	50

/* reason length of klines, parts, quits etc */
# define REASONLEN	TOPICLEN
# define AWAYLEN	TOPICLEN
# define KILLLEN	TOPICLEN

# define KEYLEN		24
# define MAXRECIPIENTS	20
# define MAXBANLENGTH	1024
# define OPERNICKLEN	(NICKLEN * 2)

# define USERHOST_REPLYLEN	(NICKLEN + HOSTLEN + USERLEN + 5)
# define MAX_DATE_STRING	32 /* maximum length for a date string. */

# define HELPLEN	400
# define IDPREFIXLEN	3

/* 
 * message return values 
 */
# define CLIENT_EXITED		(-2)
# define CLIENT_PARSE_ERROR	(-1)
# define CLIENT_OK		(1)

# ifdef IPV6
#  ifndef AF_INET6
#   error "AF_INET6 not defined"
#  endif
# else /* #ifdef IPV6 */
#  ifndef AF_INET6
#   define AF_INET6 AF_MAX /* Dummy AF_INET6 declaration */
#  endif
# endif /* #ifdef IPV6 */

# ifdef IPV6
#  define PATRICIA_BITS		128
#  define irc_sockaddr_storage	sockaddr_storage
# else
#  define PATRICIA_BITS		32
#  define irc_sockaddr_storage	sockaddr
#  define ss_family		sa_family
#  ifdef SOCKADDR_IN_HAS_LEN
#   define ss_len sa_len
#  endif
# endif

# ifdef SOCKADDR_IN_HAS_LEN
#  define SET_SS_LEN(x, y) (x).ss_len = (y)
#  define GET_SS_LEN(x) x.ss_len
# else
#  define SET_SS_LEN(x, y)
#  ifdef IPV6
#   define GET_SS_LEN(x) x.ss_family == AF_INET ? sizeof(struct sockaddr_in) : sizeof(struct sockaddr_in6)
#  else
#   define GET_SS_LEN(x) sizeof(struct sockaddr_in)
#  endif
# endif

#endif /* ! _SEVEN_IRCD_DEFS_H */

/*
 * vim: ts=8 sw=8 noet fdm=marker tw=80
 */
