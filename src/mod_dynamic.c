/* {{{ irc-seven: Cows like it.
 *
 * Copyright (C) 1990 Jarkko Oikarinen and University of Oulu, Co Center.
 * Copyright (C) 1996-2002 Hybrid Development Team.
 * Copyright (C) 2002-2005 ircd-ratbox development team.
 * Copyright (C) 2006 Elfyn McBratney.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to:
 *
 *	Free Software Foundation, Inc.
 *	51 Franklin St - Fifth Floor
 *	Boston, MA 02110-1301
 *	USA
 *
 * }}} */

/* {{{ Includes. */
#include "stdinc.h"
#include "modules.h"
#include "s_log.h"
#include "ircd.h"
#include "client.h"
#include "send.h"
#include "s_conf.h"
#include "s_newconf.h"
#include "s_serv.h"
#include "numeric.h"
#include "parse.h"
#include "ircd_defs.h"
#include "irc_string.h"
#include "memory.h"
#include "tools.h"
#include "sprintf_irc.h"
/* }}} */

# define MODS_INCREMENT 10

struct module **modlist = NULL;

/* {{{ static const char *core_module_table[] = { ... } */
# define _M(s)	#s
# define M(n)	_M(m_##n)

static const char *core_module_table[] =
{
	M(die),
	M(error),
	M(join),
	M(kick),
	M(kill),
	M(message),
	M(mode),
	M(nick),
	M(part),
	M(quit),
	M(server),
	M(sjoin),
	M(squit),
	NULL,
};
/* }}} */

int num_mods = 0;
int max_mods = MODS_INCREMENT;

static dlink_list mod_paths;

extern struct Message error_msgtab;

/* {{{ Prototypes. */
static int mo_modload (struct Client *, struct Client *, int, const char **);
static int me_modload (struct Client *, struct Client *, int, const char **);
static int mo_modunload (struct Client *, struct Client *, int, const char **);
static int me_modunload (struct Client *, struct Client *, int, const char **);
static int mo_modreload (struct Client *, struct Client *, int, const char **);
static int me_modreload (struct Client *, struct Client *, int, const char **);
static int mo_modrestart (struct Client *, struct Client *, int, const char **);
static int me_modrestart (struct Client *, struct Client *, int, const char **);
static int mo_modlist (struct Client *, struct Client *, int, const char **);
static int me_modlist (struct Client *, struct Client *, int, const char **);

static int do_modload (struct Client *, const char *);
static int do_modunload (struct Client *, const char *);
static int do_modreload (struct Client *, const char *);
static int do_modrestart (struct Client *);
static int do_modlist (struct Client *);
/* }}} */

/* {{{ static struct Message modload_msgtab = { ... } */
struct Message modload_msgtab = {
	"MODLOAD", 0, 0, 0, MFLG_SLOW,
	{
		mg_unreg, mg_not_oper, mg_ignore,
		mg_ignore, {me_modload, 1}, {mo_modload, 0},
	},
};
/* }}} */

/* {{{ static struct Message modunload_msgtab = { ... } */
struct Message modunload_msgtab =
{
	"MODUNLOAD", 0, 0, 0, MFLG_SLOW,
	{
		mg_unreg, mg_not_oper, mg_ignore,
		mg_ignore, {me_modunload, 1}, {mo_modunload, 0},
	},
};
/* }}} */

/* {{{ static struct Message modreload_msgtab = { ... } */
struct Message modreload_msgtab =
{
	"MODRELOAD", 0, 0, 0, MFLG_SLOW,
	{
		mg_unreg, mg_not_oper, mg_ignore,
		mg_ignore, {me_modreload, 1}, {mo_modreload, 0},
	},
};
/* }}} */

/* {{{ static struct Message modrestart_msgtab = { ... } */
struct Message modrestart_msgtab =
{
	"MODRESTART", 0, 0, 0, MFLG_SLOW,
	{
		mg_unreg, mg_not_oper, mg_ignore,
		mg_ignore, {me_modrestart, 0}, {mo_modrestart, 0},
	},
};
/* }}} */

/* {{{ static struct Message modlist_msgtab = { ... } */
struct Message modlist_msgtab =
{
	"MODLIST", 0, 0, 0, MFLG_SLOW,
	{
		mg_unreg, mg_not_oper, mg_ignore,
		mg_ignore, {me_modlist, 0}, {mo_modlist, 0},
	},
};
/* }}} */

/* {{{ void modules_init() */
void
modules_init (void)
{
	mod_add_cmd(&modload_msgtab);
	mod_add_cmd(&modunload_msgtab);
	mod_add_cmd(&modreload_msgtab);
	mod_add_cmd(&modlist_msgtab);
	mod_add_cmd(&modrestart_msgtab);

	/* Add the default paths we look in to the module system --nenolod */
	mod_add_path(MODPATH);
	mod_add_path(AUTOMODPATH);
}
/* }}} */

/* {{{ static struct module_path *mod_find_path()
 *
 * input	- path
 * output	- none
 * side effects - returns a module path from path
 */
static struct module_path *
mod_find_path (const char *path)
{
	dlink_node		*cur = NULL;
	struct module_path	*mpath = NULL;

	DLINK_FOREACH (cur, mod_paths.head)
	{
		mpath = cur->data;
		if(!strcmp(path, mpath->path))
			return mpath;
	}

	return NULL;
}
/* }}} */

/* {{{ void mod_add_path()
 *
 * input	- path
 * ouput	- 
 * side effects - adds path to list
 */
void
mod_add_path (const char *path)
{
	struct module_path *pathst = NULL;

	if(mod_find_path(path))
		return;

	pathst = MyMalloc(sizeof(*pathst));
	strcpy(pathst->path, path);
	dlinkAddAlloc(pathst, &mod_paths);
}
/* }}} */

/* {{{ void mod_clear_paths()
 *
 * input	-
 * output	-
 * side effects - clear the lists of paths
 */
void
mod_clear_paths (void)
{
	dlink_node *cur = NULL;
	dlink_node *next = NULL;

	DLINK_FOREACH_SAFE (cur, next, mod_paths.head)
	{
		MyFree(cur->data);
		free_dlink_node(cur);
	}

	mod_paths.head = mod_paths.tail = NULL;
	mod_paths.length = 0;
}
/* }}} */

/* {{{ char *irc_basename()
 *
 * input	-
 * output	-
 * side effects -
 */
char *
irc_basename (const char *path)
{
	char		*mod_basename = MyMalloc(strlen(path) + 1);
	const char	*s = NULL;

	s = strrchr(path, '/');
	if (!s)
		s = path;
	else
		++s;

	strcpy(mod_basename, s);
	return mod_basename;
}
/* }}} */

/* {{{ int findmodule_byname()
 *
 * input        -
 * output       -
 * side effects -
 */
int
findmodule_byname (const char *name)
{
	int i;

	for (i = 0; i < num_mods; ++i)
	{
		if(!irccmp(modlist[i]->name, name))
			return i;
	}

	return -1;
}
/* }}} */

/*  {{{ void load_all_modules()
 *
 * input        -
 * output       -
 * side effects -
 */
void
load_all_modules (int warn)
{
	DIR		*system_module_dir = NULL;
	struct dirent	*ldirent = NULL;
	char		module_fq_name[PATH_MAX + 1];
	int		len;

	modules_init();

	modlist = (struct module **) MyMalloc(sizeof(struct module) * (MODS_INCREMENT));
	max_mods = MODS_INCREMENT;
	system_module_dir = opendir(AUTOMODPATH);

	if(system_module_dir == NULL)
	{
		ilog(L_MAIN, "Could not load modules from %s: %s", AUTOMODPATH, strerror(errno));
		return;
	}

	while ((ldirent = readdir(system_module_dir)) != NULL)
	{
        	len = strlen(ldirent->d_name);
		if((len > 3) && !strcmp(ldirent->d_name+len-3, SHARED_SUFFIX))
                {
		 	(void) ircsnprintf(module_fq_name, sizeof(module_fq_name), "%s/%s", AUTOMODPATH, ldirent->d_name);
		 	(void) load_a_module(module_fq_name, warn, 0);
                }

	}

	(void) closedir(system_module_dir);
}
/* }}} */

/* {{{ void load_core_modules()
 *
 * input        -
 * output       -
 * side effects - core modules are loaded, if any fail, kill ircd
 */
void
load_core_modules (int warn)
{
	int	i;
	char	module_name[MAXPATHLEN];

	for (i = 0; core_module_table[i]; ++i)
	{
		ircsnprintf(module_name, sizeof(module_name), "%s/%s%s", MODPATH,
			    core_module_table[i], SHARED_SUFFIX);

		if(load_a_module(module_name, warn, 1) == -1)
		{
			ilog(L_MAIN,
			     "Error loading core module %s%s: terminating ircd",
			     core_module_table[i], SHARED_SUFFIX);
			exit(0);
		}
	}
}
/* }}} */

/* {{{ int load_one_module()
 *
 * input        -
 * output       -
 * side effects -
 */
int
load_one_module (const char *path, int coremodule)
{
	char			modpath[MAXPATHLEN];
	dlink_node		*cur = NULL;
	struct module_path	*mpath = NULL;
	struct stat		statbuf;

	if (server_state_foreground == 1)
		inotice("loading module %s ...", path);	

	DLINK_FOREACH (cur, mod_paths.head)
	{
		mpath = cur->data;
		ircsnprintf(modpath, sizeof(modpath), "%s/%s", mpath->path, path);

		if ((strstr(modpath, "../") == NULL) && (strstr(modpath, "/..") == NULL))
		{
			if (stat(modpath, &statbuf) == 0)
			{
				if (S_ISREG(statbuf.st_mode))
				{
					/* Regular files only please */
					if (coremodule)
						return load_a_module(modpath, 1, 1);
					else
						return load_a_module(modpath, 1, 0);
				}
			}

		}
	}

	sendto_realops_snomask(SNO_GENERAL, L_ALL, "Cannot locate module %s", path);
	return -1;
}
/* }}} */

/* {{{ static int mo_modload()
 *
 * Load a module into the IRCd.
 *
 * Usage:
 *	MODLOAD <module> [ON <server>]
 *
 * parv[0] - prefix
 * parv[1] - module
 * parv[2] - "ON", or nothing
 * parv[3] - server, or nothing
 */
static int
mo_modload (struct Client *client_p, struct Client *source_p, int parc, const char **parv)
{
	if (!IsOperAdmin(source_p))
	{
		sendto_one(source_p, form_str(ERR_NOPRIVS),
			   me.name, source_p->name, "admin");
		return 0;
	}
	else if (parc > 4)
	{
		sendto_one(source_p,
			":%s NOTICE %s :To many parameters to MODLOAD",
			me.name, source_p->name);
		return 0;
	}
	else if ((parc != 2 && parc != 4) || EmptyString(parv[1]))
	{
		sendto_one(source_p, form_str(ERR_NEEDMOREPARAMS), me.name,
			source_p->name, "MODLOAD");
		return 0;
	}

	/* remote? */
	if (parc == 4)
	{
		if (EmptyString(parv[2]) | EmptyString(parv[3]))
		{
			sendto_one(source_p, form_str(ERR_NEEDMOREPARAMS), me.name,
				source_p->name, "MODLOAD");
			return 0;
		}
		else if (irccmp(parv[2], "ON") != 0)
		{
			sendto_one(source_p,
				":%s NOTICE %s :Second argument to MODLOAD "
				"must be [ON] not [%s]",
				me.name, source_p->name, parv[2]);
			return 0;
		}

		sendto_match_servs(source_p, parv[3], CAP_ENCAP, NOCAPS,
			"ENCAP %s MODLOAD :%s", parv[3], parv[1]);

		if (!match(parv[3], me.name))
			return 0;
	}

	return do_modload(source_p, parv[1]);
}
/* }}} */

/* {{{ static int me_modload()
 *
 * MODLOAD ENCAP message handler.
 *
 * Usage:
 *	MODLOAD <module>
 *
 * parv[0] - prefix
 * parv[1] - module
 */
static int
me_modload (struct Client *client_p, struct Client *source_p, int parc, const char **parv)
{
	if (!IsPerson(source_p) || EmptyString(parv[1]))
		return 0;
	if (!find_client_shared_conf(source_p, SHARED_MODULE))
		return 0;

	return do_modload(source_p, parv[1]);
}
/* }}} */

/* {{{ static int do_modload()
 *
 * Perform actual loading of modules into the IRCd.
 */
static int
do_modload (struct Client *source_p, const char *path)
{
	char *base = NULL;

	base = irc_basename(path);
	if (findmodule_byname(base) != -1)
	{
		sendto_one(source_p,
			":%s NOTICE %s :Module %s is already loaded",
			me.name, source_p->name, base);

		MyFree(base);
		return 0;
	}

	load_one_module(path, 0);
	MyFree(base);

	return 0;
}
/* }}} */

/* {{{ static int mo_modunload()
 *
 * Unload a module from the IRCd.
 *
 * Usage:
 *	MODUNLOAD <module>
 *
 * parv[0] - prefix
 * parv[1] - module
 */
static int
mo_modunload(struct Client *client_p, struct Client *source_p, int parc, const char **parv)
{
	if (!IsOperAdmin(source_p))
	{
		sendto_one(source_p, form_str(ERR_NOPRIVS),
			   me.name, source_p->name, "admin");
		return 0;
	}
	else if (parc > 4)
	{
		sendto_one(source_p,
			":%s NOTICE %s :To many parameters to MODUNLOAD",
			me.name, source_p->name);
		return 0;
	}
	else if ((parc != 2 && parc != 4) || EmptyString(parv[1]))
	{
		sendto_one(source_p, form_str(ERR_NEEDMOREPARAMS), me.name,
			source_p->name, "MODUNLOAD");
		return 0;
	}

	/* remote? */
	if (parc == 4)
	{
		if (EmptyString(parv[2]) | EmptyString(parv[3]))
		{
			sendto_one(source_p, form_str(ERR_NEEDMOREPARAMS), me.name,
				source_p->name, "MODUNLOAD");
			return 0;
		}
		else if (irccmp(parv[2], "ON") != 0)
		{
			sendto_one(source_p,
				":%s NOTICE %s :Second argument to MODUNLOAD "
				"must be [ON] not [%s]",
				me.name, source_p->name, parv[2]);
			return 0;
		}

		sendto_match_servs(source_p, parv[3], CAP_ENCAP, NOCAPS,
			"ENCAP %s MODUNLOAD :%s", parv[3], parv[1]);

		if (!match(parv[3], me.name))
			return 0;
	}

	return do_modunload(source_p, parv[1]);
}
/* }}} */

/* {{{ static int me_modunload()
 *
 * MODUNLOAD ENCAP message handler.
 *
 * Usage:
 *	MODUNLOAD <module>
 *
 * parv[0] - prefix
 * parv[1] - module
 */
static int
me_modunload (struct Client *client_p, struct Client *source_p, int parc, const char **parv)
{
	if (!IsPerson(source_p) || EmptyString(parv[1]))
		return 0;
	if (!find_client_shared_conf(source_p, SHARED_MODULE))
		return 0;

	return do_modunload(source_p, parv[1]);
}
/* }}} */

/* {{{ static int do_modunload()
 *
 * Perform actual unloading of modules.
 */
static int
do_modunload (struct Client *source_p, const char *path)
{
	char	*base = NULL;
	int	i;

	base = irc_basename(path);
	if ((i = findmodule_byname(base)) < 0)
	{
		sendto_one(source_p,
			":%s NOTICE %s :Module %s is not loaded",
			me.name, source_p->name, base);

		MyFree(base);
		return 0;
	}

	if (modlist[i]->core == 1)
	{
		sendto_one(source_p,
			":%s NOTICE %s :Module %s is a core module and cannot be unloaded",
			me.name, source_p->name, base);

		MyFree(base);
		return 0;
	}

	if (unload_one_module(base, 1) < 0)
		sendto_one(source_p,
			":%s NOTICE %s :Module %s is not loaded",
			me.name, source_p->name, base);

	MyFree(base);
	return 0;
}
/* }}} */

/* {{{ static int mo_modreload()
 *
 * Reload (i.e., unload and then load) a module.
 *
 * Usage:
 *	MODRELOAD <module> [ON <server>]
 *
 * parv[0] - prefix
 * parv[1] - module
 * parv[2] - "ON", or nothing
 * parv[3] - server, or nothing
 */
static int
mo_modreload (struct Client *client_p, struct Client *source_p, int parc, const char **parv)
{
	if (!IsOperAdmin(source_p))
	{
		sendto_one(source_p, form_str(ERR_NOPRIVS),
			   me.name, source_p->name, "admin");
		return 0;
	}
	else if (parc > 4)
	{
		sendto_one(source_p,
			":%s NOTICE %s :To many parameters to MODRELOAD",
			me.name, source_p->name);
		return 0;
	}
	else if ((parc != 2 && parc != 4) || EmptyString(parv[1]))
	{
		sendto_one(source_p, form_str(ERR_NEEDMOREPARAMS), me.name,
			source_p->name, "MODRELOAD");
		return 0;
	}

	/* remote? */
	if (parc == 4)
	{
		if (EmptyString(parv[2]) | EmptyString(parv[3]))
		{
			sendto_one(source_p, form_str(ERR_NEEDMOREPARAMS), me.name,
				source_p->name, "MODRELOAD");
			return 0;
		}
		else if (irccmp(parv[2], "ON") != 0)
		{
			sendto_one(source_p,
				":%s NOTICE %s :Second argument to MODRELOAD "
				"must be [ON] not [%s]",
				me.name, source_p->name, parv[2]);
			return 0;
		}

		sendto_match_servs(source_p, parv[3], CAP_ENCAP, NOCAPS,
			"ENCAP %s MODRELOAD :%s", parv[3], parv[1]);

		if (!match(parv[3], me.name))
			return 0;
	}

	return do_modreload(source_p, parv[1]);
}
/* }}} */

/* {{{ static int me_modreload()
 *
 * MODRELOAD ENCAP message handler.
 *
 * Usage:
 *	MODRELOAD <module>
 *
 * parv[0] - prefix
 * parv[1] - module
 */
static int
me_modreload (struct Client *client_p, struct Client *source_p, int parc, const char **parv)
{
	if (!IsPerson(source_p) || EmptyString(parv[1]))
		return 0;
	if (!find_client_shared_conf(source_p, SHARED_MODULE))
		return 0;

	return do_modreload(source_p, parv[1]);
}
/* }}} */

/* {{{ static int do_modreload()
 *
 * Perform actual reloading of modules.
 */
static int
do_modreload (struct Client *source_p, const char *path)
{
	char	*base = NULL;
	int	i;
	int	is_core;

	base = irc_basename(path);
	if ((i = findmodule_byname(base)) < 0)
	{
		sendto_one(source_p,
			":%s NOTICE %s :Module %s is not loaded",
			me.name, source_p->name, base);

		MyFree(base);
		return 0;
	}

	is_core = modlist[i]->core;
	if (unload_one_module(base, 1) < 0)
	{
		sendto_one(source_p,
			":%s NOTICE %s :Module %s is not loaded",
			me.name, source_p->name, base);

		MyFree(base);
		return 0;
	}

	if (load_one_module(path, is_core) < 0 && is_core)
	{
		sendto_realops_snomask(SNO_GENERAL, L_NETWIDE,
			"Error while reloading core module: %s; terminating ircd",
			path);

		ilog(L_MAIN,
			"Error while reloading core module: %s; terminating ircd",
			path);
		exit(0);
	}

	MyFree(base);
	return 0;
}
/* }}} */

/* {{{ static int mo_modrestart()
 *
 * Reload all modules.
 *
 * Usage:
 *	MODRESTART [ON <server>]
 *
 * parv[0] - prefix
 * parv[1] - "ON", or nothing
 * parv[2] - server, or nothing
 */
static int
mo_modrestart (struct Client *client_p, struct Client *source_p, int parc, const char **parv)
{
	if (!IsOperAdmin(source_p))
	{
		sendto_one(source_p, form_str(ERR_NOPRIVS),
			   me.name, source_p->name, "admin");
		return 0;
	}
	else if (parc > 3)
	{
		sendto_one(source_p,
			":%s NOTICE %s :To many parameters to MODRESTART",
			me.name, source_p->name);
		return 0;
	}
	else if (parc == 2 || (parc == 3 && (EmptyString(parv[1]) || EmptyString(parv[2]))))
	{
		sendto_one(source_p, form_str(ERR_NEEDMOREPARAMS), me.name,
			source_p->name, "MODRESTART");
		return 0;
	}

	if (parc == 3)
	{
		if (irccmp(parv[1], "ON") != 0)
		{
			sendto_one(source_p,
				":%s NOTICE %s :Second argument to MODRESTART "
				"must be [ON] not [%s]",
				me.name, source_p->name, parv[1]);
			return 0;
		}
		
		sendto_match_servs(source_p, parv[2], CAP_ENCAP, NOCAPS,
			"ENCAP %s MODRESTART", parv[2]);

		if (!match(parv[2], me.name))
			return 0;
	}

	return do_modrestart(source_p);
}
/* }}} */

/* {{{ static int me_modrestart()
 *
 * MODRESTART ENCAP message handler.
 *
 * Usage:
 *	MODRESTART
 *
 * parv[0] - prefix
 */
static int
me_modrestart (struct Client *client_p, struct Client *source_p, int parc, const char **parv)
{
	if (!IsPerson(source_p))
		return 0;
	if (!find_client_shared_conf(source_p, SHARED_MODULE))
		return 0;

	return do_modrestart(source_p);
}
/* }}} */

/* {{{ static int do_modrestart()
 *
 * Perform actual restart of all modules.
 */
static int
do_modrestart (struct Client *source_p)
{
	int n;

	sendto_one(source_p, ":%s NOTICE %s :Reloading all modules",
		me.name, source_p->name);

	n = num_mods;
	while (num_mods)
		unload_one_module(modlist[0]->name, 0);

	load_all_modules(0);
	load_core_modules(0);
	rehash(0);

	sendto_realops_snomask(SNO_GENERAL, L_ALL,
		"Module Restart: %d modules unloaded, %d modules loaded",
		n, num_mods);
	ilog(L_MAIN, "Module Restart: %d modules unloaded, %d modules loaded",
		n, num_mods);

	return 0;
}
/* }}} */

/* {{{ static int mo_modlist()
 *
 * List modules currently loaded into the IRCd.
 *
 * Usage:
 *	MODLIST [<module>] [ON <server>]
 *
 * parv[0] - prefix
 * parv[2] - "ON", or nothing
 * parv[3] - server, or nothing
 */
static int
mo_modlist (struct Client *client_p, struct Client *source_p, int parc, const char **parv)
{
	if (!IsOperAdmin(source_p))
	{
		sendto_one(source_p, form_str(ERR_NOPRIVS),
			   me.name, source_p->name, "admin");
		return 0;
	}
	else if (parc > 3)
	{
		sendto_one(source_p,
			":%s NOTICE %s :To many parameters to MODLIST",
			me.name, source_p->name);
		return 0;
	}
	else if (parc == 2 || (parc == 3 && (EmptyString(parv[1]) || EmptyString(parv[2]))))
	{
		sendto_one(source_p, form_str(ERR_NEEDMOREPARAMS), me.name,
			source_p->name, "MODLIST");
		return 0;
	}

	if (parc == 3)
	{
		if (irccmp(parv[1], "ON") != 0)
		{
			sendto_one(source_p,
				":%s NOTICE %s :Second argument to MODLIST "
				"must be [ON] not [%s]",
				me.name, source_p->name, parv[1]);
			return 0;
		}
		
		sendto_match_servs(source_p, parv[2], CAP_ENCAP, NOCAPS,
			"ENCAP %s MODLIST", parv[2]);

		if (!match(parv[2], me.name))
			return 0;
	}

	return do_modlist(source_p);
}
/* }}} */

/* {{{ static int me_modlist()
 *
 * MODLIST ENCAP message handler.
 *
 * Usage:
 *	MODLIST
 *
 * parv[0] - prefix
 */
static int
me_modlist (struct Client *client_p, struct Client *source_p, int parc, const char **parv)
{
	if (!IsPerson(source_p))
		return 0;
	if (!find_client_shared_conf(source_p, SHARED_MODULE))
		return 0;

	return do_modlist(source_p);
}
/* }}} */

/* {{{ static int do_modlist()
 *
 * Perform actual listing of loaded in modules.
 */
static int
do_modlist (struct Client *source_p)
{
	int i;

	for (i = 0; i < num_mods; ++i)
	{
		sendto_one(source_p, form_str(RPL_MODLIST), me.name,
			source_p->name, modlist[i]->name,
			modlist[i]->address, modlist[i]->version,
			modlist[i]->core ? "(core)" : "");
	}

	sendto_one(source_p, form_str(RPL_ENDOFMODLIST), me.name,
		source_p->name);

	return 0;
}
/* }}} */

# ifndef RTLD_NOW
#  define RTLD_NOW RTLD_LAZY	/* openbsd deficiency */
# endif

# ifdef SEVEN_PROFILE
#  ifndef RTLD_PROFILE
#   warning libdl may not support profiling, sucks. :(
#   define RTLD_PROFILE 0
#  endif
# endif

static void increase_modlist(void);

static char unknown_ver[] = "<unknown>";

/* {{{ int unload_one_module()
 *
 * inputs	- name of module to unload
 *		- 1 to say modules unloaded, 0 to not
 * output	- 0 if successful, -1 if error
 * side effects	- module is unloaded
 */
int
unload_one_module(const char *name, int warn)
{
	int modindex;

	if((modindex = findmodule_byname(name)) == -1)
		return -1;

	/*
	 ** XXX - The type system in C does not allow direct conversion between
	 ** data and function pointers, but as it happens, most C compilers will
	 ** safely do this, however it is a theoretical overlow to cast as we 
	 ** must do here.  I have library functions to take care of this, but 
	 ** despite being more "correct" for the C language, this is more 
	 ** practical.  Removing the abuse of the ability to cast ANY pointer
	 ** to and from an integer value here will break some compilers.
	 **          -jmallett
	 */
	/* Left the comment in but the code isn't here any more         -larne */
	switch (modlist[modindex]->mapi_version)
	{
	case 1:
		{
			struct mapi_mheader_av1 *mheader = modlist[modindex]->mapi_header;
			if(mheader->mapi_command_list)
			{
				struct Message **m;
				for (m = mheader->mapi_command_list; *m; ++m)
					mod_del_cmd(*m);
			}

			/* hook events are never removed, we simply lose the
			 * ability to call them --fl
			 */
			if(mheader->mapi_hfn_list)
			{
				mapi_hfn_list_av1 *m;
				for (m = mheader->mapi_hfn_list; m->hapi_name; ++m)
					remove_hook(m->hapi_name, m->fn);
			}

			if(mheader->mapi_unregister)
				mheader->mapi_unregister();
			break;
		}
	default:
		sendto_realops_snomask(SNO_GENERAL, L_ALL,
				     "Unknown/unsupported MAPI version %d when unloading %s!",
				     modlist[modindex]->mapi_version, modlist[modindex]->name);
		ilog(L_MAIN, "Unknown/unsupported MAPI version %d when unloading %s!",
		     modlist[modindex]->mapi_version, modlist[modindex]->name);
		break;
	}

	dlclose(modlist[modindex]->address);

	MyFree(modlist[modindex]->name);
	memcpy(&modlist[modindex], &modlist[modindex + 1],
	       sizeof(struct module) * ((num_mods - 1) - modindex));

	if(num_mods != 0)
		num_mods--;

	if(warn == 1)
	{
		ilog(L_MAIN, "Module %s unloaded", name);
		sendto_realops_snomask(SNO_GENERAL, L_ALL, "Module %s unloaded", name);
	}

	return 0;
}
/* }}} */

/* {{{ int load_a_module()
 *
 * inputs	- path name of module, int to notice, int of core
 * output	- -1 if error 0 if success
 * side effects - loads a module if successful
 */
int
load_a_module(const char *path, int warn, int core)
{
	void *tmpptr = NULL;

	char *mod_basename;
	const char *ver;

	int *mapi_version;

	mod_basename = irc_basename(path);

#ifdef SEVEN_PROFILE
	tmpptr = dlopen(path, RTLD_NOW | RTLD_PROFILE);
#else
	tmpptr = dlopen(path, RTLD_NOW);
#endif

	if(tmpptr == NULL)
	{
		const char *err = dlerror();

		sendto_realops_snomask(SNO_GENERAL, L_ALL,
				     "Error loading module %s: %s", mod_basename, err);
		ilog(L_MAIN, "Error loading module %s: %s", mod_basename, err);
		MyFree(mod_basename);
		return -1;
	}


	/*
	 * _mheader is actually a struct mapi_mheader_*, but mapi_version
	 * is always the first member of this structure, so we treate it
	 * as a single int in order to determine the API version.
	 *      -larne.
	 */
	mapi_version = (int *) (uintptr_t) dlsym(tmpptr, "_mheader");
	if((mapi_version == NULL
	    && (mapi_version = (int *) (uintptr_t) dlsym(tmpptr, "__mheader")) == NULL)
	   || MAPI_MAGIC(*mapi_version) != MAPI_MAGIC_HDR)
	{
		sendto_realops_snomask(SNO_GENERAL, L_ALL,
				     "Data format error: module %s has no MAPI header.",
				     mod_basename);
		ilog(L_MAIN, "Data format error: module %s has no MAPI header.", mod_basename);
		(void) dlclose(tmpptr);
		MyFree(mod_basename);
		return -1;
	}

	switch (MAPI_VERSION(*mapi_version))
	{
	case 1:
		{
			struct mapi_mheader_av1 *mheader = (struct mapi_mheader_av1 *) mapi_version;	/* see above */
			if(mheader->mapi_register && (mheader->mapi_register() == -1))
			{
				ilog(L_MAIN, "Module %s indicated failure during load.",
				     mod_basename);
				sendto_realops_snomask(SNO_GENERAL, L_ALL,
						     "Module %s indicated failure during load.",
						     mod_basename);
				dlclose(tmpptr);
				MyFree(mod_basename);
				return -1;
			}
			if(mheader->mapi_command_list)
			{
				struct Message **m;
				for (m = mheader->mapi_command_list; *m; ++m)
					mod_add_cmd(*m);
			}

			if(mheader->mapi_hook_list)
			{
				mapi_hlist_av1 *m;
				for (m = mheader->mapi_hook_list; m->hapi_name; ++m)
					*m->hapi_id = register_hook(m->hapi_name);
			}

			if(mheader->mapi_hfn_list)
			{
				mapi_hfn_list_av1 *m;
				for (m = mheader->mapi_hfn_list; m->hapi_name; ++m)
					add_hook(m->hapi_name, m->fn);
			}

			ver = mheader->mapi_module_version;
			break;
		}

	default:
		ilog(L_MAIN, "Module %s has unknown/unsupported MAPI version %d.",
		     mod_basename, MAPI_VERSION(*mapi_version));
		sendto_realops_snomask(SNO_GENERAL, L_ALL,
				     "Module %s has unknown/unsupported MAPI version %d.",
				     mod_basename, *mapi_version);
		dlclose(tmpptr);
		MyFree(mod_basename);
		return -1;
	}

	if(ver == NULL)
		ver = unknown_ver;

	increase_modlist();

	modlist[num_mods] = MyMalloc(sizeof(struct module));
	modlist[num_mods]->address = tmpptr;
	modlist[num_mods]->version = ver;
	modlist[num_mods]->core = core;
	DupString(modlist[num_mods]->name, mod_basename);
	modlist[num_mods]->mapi_header = mapi_version;
	modlist[num_mods]->mapi_version = MAPI_VERSION(*mapi_version);
	num_mods++;

	if(warn == 1)
	{
		sendto_realops_snomask(SNO_GENERAL, L_ALL,
				     "Module %s [version: %s; MAPI version: %d] loaded at 0x%lx",
				     mod_basename, ver, MAPI_VERSION(*mapi_version),
				     (unsigned long) tmpptr);
		ilog(L_MAIN, "Module %s [version: %s; MAPI version: %d] loaded at 0x%lx",
		     mod_basename, ver, MAPI_VERSION(*mapi_version), (unsigned long) tmpptr);
	}
	MyFree(mod_basename);
	return 0;
}
/* }}} */

/* {{{ static void increase_modlist()
 *
 * inputs	- NONE
 * output	- NONE
 * side effects	- expand the size of modlist if necessary
 */
static void
increase_modlist(void)
{
	struct module **new_modlist = NULL;

	if((num_mods + 1) < max_mods)
		return;

	new_modlist = (struct module **) MyMalloc(sizeof(struct module) *
						  (max_mods + MODS_INCREMENT));
	memcpy((void *) new_modlist, (void *) modlist, sizeof(struct module) * num_mods);

	MyFree(modlist);
	modlist = new_modlist;
	max_mods += MODS_INCREMENT;
}
/* }}} */

/*
 * vim: ts=8 sw=8 noet fdm=marker tw=80
 */
